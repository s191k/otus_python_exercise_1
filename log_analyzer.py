#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import datetime
import gzip
import io
import json
import os
import re
import sys
import logging
from string import Template

config = {
    "REPORT_SIZE": 1000,  # Количество строк с наибольшим time
    "REPORT_DIR": "reports",  # Откуда забирать архивы
    "LOG_DIR": "log",  # Куда писать html репорты
    "ERROR_MAX_PROCENT": 35,
    "LOGGER_DIR": "log/log_work/" # Дефолтная папка логгирования
}


def get_logger_file_name(globalconfig): #Создание папки для логгера
    if not os.path.exists(globalconfig['LOGGER_DIR']): os.makedirs(globalconfig['LOGGER_DIR'],exist_ok=True)
    return globalconfig['LOGGER_DIR'] + datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d') + ".log"

def parse_line(line, summary_url_count, summary_request_time, result_map):
    """Парсим линию из лога.
    Вовзращаем данные, чтобы подсчитать общую статистику."""
    logging.info(msg=line)
    splitted_cur_line = line.replace('"', '').split()
    cur_url = splitted_cur_line[6]
    cur_request_time = float(splitted_cur_line[-1])
    summary_url_count += 1
    summary_request_time += cur_request_time
    if cur_url not in result_map.keys():
        result_map[cur_url] = [cur_request_time]
    else:
        cur_values = result_map.get(cur_url)
        cur_values.append(cur_request_time)
        result_map[cur_url] = cur_values
    return summary_url_count, summary_request_time, result_map

def open_log_file(log_dir_path, nginx_last_log, globalconfig):
    summary_url_count = 0
    summary_request_time = 0.0
    result_map = {}
    max_error_percent = globalconfig['ERROR_MAX_PROCENT']

    open_func = gzip.open if nginx_last_log.endswith('gz') else io.open
    with open_func(os.path.join(os.getcwd(), log_dir_path, nginx_last_log), 'rb') as file:
        count = 0
        error_count = 0
        for line in file:
            count += 1
            try:
                line = line.decode('utf-8')
            except Exception as ex:
                logging.exception(ex)
                error_count += 1
            summary_url_count, summary_request_time, result_map = parse_line(
                line, summary_url_count, summary_request_time, result_map)

        if error_count > 0 and error_count / count >= max_error_percent*0.01:
            logging.error('too many errors while parsing log file')
            sys.exit()

    return result_map, summary_url_count, summary_request_time

def args_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config')
    try:
        return parser.parse_args().config
    except Exception as ex:
        logging.error(ex)

def get_config(global_config):
    """Check --config argument. If he present, replace config parametres"""
    config_arg = args_parser() #Если есть путь к конфигу - обрабатываем, иначе возращаем глобальную переменную config
    if config_arg is not None:
        config_arg_path = os.path.join(config_arg)
        with open(config_arg_path) as config_file:
            config_file_map = json.load(config_file)
            global_config.update({global_config['REPORT_SIZE']:config_file_map.get('REPORT_SIZE')},
                                 {global_config['REPORT_DIR']:config_file_map.get('REPORT_DIR')},
                                 {global_config['LOG_DIR']: config_file_map.get('LOG_DIR')},
                                 {global_config['ERROR_MAX_PROCENT']:config_file_map.get('ERROR_MAX_PROCENT')},
                                 {global_config['LOGGER_DIR']: config_file_map.get('LOGGER_DIR')})
    return global_config


def get_log_file_datetime(log_file_name):
    """Convert end of log_file_name to datetime """
    return datetime.datetime.strptime(re.search(r'[\d]{8}',log_file_name).group(),'%Y%m%d').date()

def check_exist_of_report(report_date=None, report_dir_path=None):
    """check for ready report.html for current date"""
    report_date = datetime.datetime.strptime(report_date, '%Y-%m-%d').strftime('%Y.%m.%d')
    return os.path.exists(os.path.join(report_dir_path,'report-' + report_date + '.html'))

def get_last_log(log_dir_path):
    last_log = None
    last_date = None
    for filename in os.listdir(log_dir_path):
        match = re.search(r"^nginx-access-ui\.log-(\d{8})(\.gz)?$", filename)
        if match:
            date, ext = match.groups()
            log_date = datetime.datetime.strptime(date, "%Y%m%d").date()
            if not last_log or last_date < log_date:
                last_log = filename
                last_date = log_date
    if last_log is None:
        logging.info('Отсутсвуют log файлы')
    else:
        return last_log, last_date

def create_json_string(report_size,sum_time_array, result_map_json):
    """Составляем результирующую json строку, которую будем отображать на web-странице."""
    count_of_urls = 0
    output_json_string = '['
    for x in sum_time_array:
        count_of_urls += 1
        if count_of_urls > report_size:
            break
        output_json_string += str(result_map_json.get(x[0])) + ','
    output_json_string += ']'
    return output_json_string

def create_html_report(report_dir_path,report_size,sum_time_array,
                       result_map_json,nginx_last_log):
    """Составляем результирующую web-страницу."""
    output_json_string = create_json_string(report_size=report_size, sum_time_array=sum_time_array,
                                            result_map_json=result_map_json)
    log_datetime = get_log_file_datetime(nginx_last_log)
    report_file_name = 'report-' + log_datetime.strftime('%Y.%m.%d') + '.html'

    with open(os.path.join(os.getcwd(), 'report.html'), 'r') as report_html_file:
        with open(os.path.join(os.getcwd(), report_dir_path, report_file_name), 'w') as new_report_html_file:
            src = Template(report_html_file.read())
            result = src.safe_substitute(table_json=output_json_string) ## ; ?????
            new_report_html_file.write(result)


def consider_median(count_of_current_array, array_of_request_time_current_url):
    """Высчитываем медиану."""
    if count_of_current_array % 2 != 0 and count_of_current_array > 2:
        median = array_of_request_time_current_url[round(count_of_current_array / 2)]
    elif count_of_current_array % 2 == 0 and count_of_current_array > 2:
        median = (array_of_request_time_current_url[int(count_of_current_array / 2)] +
                   array_of_request_time_current_url[int(count_of_current_array / 2) + 1])
    elif count_of_current_array == 2:
        median = ((array_of_request_time_current_url[0] + array_of_request_time_current_url[1]) / 2)
    else:  # если только 1 число
        median = array_of_request_time_current_url[0]
    return median


def create_result_json_map(keys, result_map,summary_url_count,summary_request_time):
    """Составляем итоговую json карту(словарь)."""
    result_map_json = {}
    for current_url in keys:
        array_of_current_url_request_time = sorted(result_map[current_url])

        count_of_current_array = len(array_of_current_url_request_time)
        request_total_time_current_url = 0.0
        for request_time_for_current_url in array_of_current_url_request_time:
            request_total_time_current_url += request_time_for_current_url

        median = consider_median(count_of_current_array=count_of_current_array,
                                  array_of_request_time_current_url=array_of_current_url_request_time)
        result_map_json[current_url] = {'count': count_of_current_array,
                              'count_perc': round(float(count_of_current_array /
                                                        summary_url_count), 3),  # в конце посчитать
                              'time_sum': round(request_total_time_current_url, 3),
                              'time_perc': round(float(request_total_time_current_url /
                                                       summary_request_time), 3),  # в конце посчитать
                              'time_avg': round(float(request_total_time_current_url /
                                                      count_of_current_array), 3),
                              'time_max': round(array_of_current_url_request_time[-1], 3),
                              'time_med': round(median, 3),
                              'url': current_url}
    return result_map_json

def main():
    try:
        result_map = {}
        summary_url_count = 0
        summary_request_time = 0.0

        result_config = get_config(config)
        log_dir_path = result_config['LOG_DIR']
        report_dir_path = result_config['REPORT_DIR']
        report_size = result_config['REPORT_SIZE']

        logging.basicConfig(level=logging.INFO, datefmt='%Y.%m.%d %H:%M:%S', filename=get_logger_file_name(config),
                            format='[%(asctime)s] %(levelname)s %(message)s')


        nginx_last_log, last_log_datetime = get_last_log(log_dir_path=log_dir_path)

        # if report exist => exit
        if check_exist_of_report(report_date=str(last_log_datetime), report_dir_path=report_dir_path):
            logging.info('report already exist')
            return
        try:
            result_map, summary_url_count, summary_request_time = open_log_file(log_dir_path=log_dir_path,
                                                                                nginx_last_log=nginx_last_log,
                                                                                globalconfig=config)
        except Exception as ex:
            logging.error(ex)

        keys = list(result_map.keys())
        result_map_json = create_result_json_map(keys=keys, result_map=result_map, summary_url_count=summary_url_count,
                          summary_request_time=summary_request_time)

        # Отсоритровать записи по $request_sum
        sum_time_array = sorted(result_map_json.items(), key=lambda value: value[1]['time_sum'], reverse=True)
        sum_time_array = sum_time_array[:report_size] if len(sum_time_array) > report_size else sum_time_array

        create_html_report(report_dir_path=report_dir_path,report_size=report_size,sum_time_array=sum_time_array,
                           result_map_json=result_map_json,nginx_last_log=nginx_last_log)

    except Exception as ex:
        logging.exception(ex)


if __name__ == "__main__":
    main()
