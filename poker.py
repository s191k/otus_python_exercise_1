#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q), король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH - дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertoolsю
# Можно свободно определять свои функции и т.п.
# -----------------
import itertools


def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks), kind(2, ranks))
    elif flush(hand):
        return (5, ranks)
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        return (3, kind(3, ranks), ranks)
    elif two_pair(ranks):
        return (2, two_pair(ranks), ranks)
    elif kind(2, ranks):
        return (1, kind(2, ranks), ranks)
    else:
        return (0, ranks)


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
        отсортированный от большего к меньшему"""
    ranks_out = []
    for x in hand: ranks_out.append(get_card_rank(x))
    ranks_out = sorted(ranks_out)
    return ranks_out

def get_card_rank(card):
    x = card[:-1]
    try:
        return int(x)
    except:
        if x == 'T': return 10
        elif x == 'J': return 11
        elif x == 'Q': return 12
        elif x == 'K': return 13
        elif x == 'A': return 14
        elif x == '?': return 15


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    count = 0
    suit = None
    for x in hand:
        if count == 0:
            suit = x[:-1]
            continue
        elif x[:-1] != suit:
            return False
        suit = x[:-1]
    return True


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность 5ти,
    где у 5ти карт ранги идут по порядку (стрит)"""
    is_straight=True
    previous = ranks[0]
    for x in ranks:
        if x == ranks[0]:continue
        else:
            if x > previous:
                previous = x
            else:
                return False
    return is_straight


def kind(n, ranks):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    for x in ranks:
        if ranks.count(x) == n:
            return x


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""
    for x in ranks:
        x_power=x[:-1]
        for y in ranks:
            if x == y:
                continue
            elif x_power == y[:-1]:
                print(x_power + '  ' + y)
    return


def sort_hand(hand):
    """ Сортируем карты методом пузырька """
    for i in range(len(hand)):
        for j in range(len(hand) - 1, i, -1):
            if get_card_rank(hand[j]) < get_card_rank(hand[j - 1]):
                hand[j], hand[j - 1] = hand[j - 1], hand[j]
    return hand


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """

    hand = sort_hand(hand)
    rank = hand_rank(hand)

    print('rank : ' + str(rank))

    best_hand_array = []
    if rank[0] == 8 or rank[0] == 6 or rank[0] == 5 or rank[0] == 4 or rank[0] == 1:
        best_hand_array = sort_hand(hand[-5:])
    elif rank[0] == 7:
        # copy - differepnt positions
        was_high_positions = False
        for x in hand:
            if get_card_rank(x) == rank[1]:
                best_hand_array.append(x)
                if x == hand[-1]:
                    was_high_positions = True
        if was_high_positions:
            best_hand_array.append(hand[-5])
        else:
            best_hand_array.append(hand[-1])
    elif rank[0] == 3:
        # copy - differepnt positions
        was_high_positions = False
        for x in hand:
            if get_card_rank(x) == rank[1]:
                best_hand_array.append(x)
                if x == hand[-1]:
                    was_high_positions = True
        if was_high_positions:
            best_hand_array.append(hand[-5:-3])
        else:
            best_hand_array.append(hand[-2:])
    elif rank[0] == 2:
        pass
        for x in hand:
            if get_card_rank(x) == rank[1] or get_card_rank(x) == rank[2]:
                best_hand_array.append(x)
        for x in hand[::-1]:
            if x not in best_hand_array:
                best_hand_array.append(x)
                return best_hand_array
    else:
        best_hand_array = sort_hand(hand[-5:])


    print('best_hand_array : '  + str(best_hand_array))
    return best_hand_array

# undone
def best_wild_hand(hand):
    """best_hand но с джокерами"""
    return best_hand(hand)


def test_best_hand():
    print("test_best_hand...")
    assert ((best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['7C', '8C', '9C', 'TC', 'JS'])
            # == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')

def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


if __name__ == '__main__':
    test_best_hand()
    # test_best_wild_hand()
