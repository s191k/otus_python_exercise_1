import datetime
import unittest

import log_analyzer


class MyTestCase(unittest.TestCase):

    @unittest.expectedFailure
    def test_get_log_file_datetime_method_bad(self):
        """Test for bad use log_analyzer.get_log_file_datetime method"""
        log_analyzer.get_log_file_datetime('nginx-access-ui.log-20170635.gz')

    def test_get_log_file_datetime_method_good(self):
        """Test for good use log_analyzer.get_log_file_datetime method"""
        datetime_from_fileName = log_analyzer.get_log_file_datetime('nginx-access-ui.log-20170630.gz')
        datetime_for_test = datetime.datetime(year=2017, month=6, day=30)
        self.assertEqual(datetime_from_fileName, datetime_for_test)

    # def test_check_exist_of_report_good(self):
    #     """if nginx-access-ui.log-20170630.gz exist, and report by this log was created, test will be passed"""
    #     result = log_analyzer.check_exist_of_report(nginx_last_log='nginx-access-ui.log-20170630.gz',
    #                                                 report_dir_path='reports')
    #     self.assertEqual(True, result)

    # def test_check_exist_of_report_bad(self):
        # """if use nonexistent log, test will be passed"""
        # result = log_analyzer.check_exist_of_report(nginx_last_log='nginx-access-ui.log-20170629.gz',
        #                                             report_dir_path='reports')
        # self.assertEqual(False,result)

    def test_get_last_log_good(self):
        """If log_dir_path has logs"""
        nginx_last_log = log_analyzer.get_last_log(log_dir_path='log')
        self.assertEqual(True, nginx_last_log is not None)

    def test_get_last_log_bad(self):
        """If log_dir_path has more than 2 logs. Need replace '20170623.gz' for you variation"""
        nginx_last_log = log_analyzer.get_last_log(log_dir_path='log')
        self.assertEqual(False, nginx_last_log.endswith('20170623.gz'))

if __name__ == '__main__':
    """run all tests"""
    unittest.main()
