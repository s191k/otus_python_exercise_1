#!/usr/bin/env python
# -*- coding: utf-8 -*-
from functools import update_wrapper, wraps


def disable(func):
    """
    Disable a decorator by re-assigning the decorator's name
    to this function. For example, to turn off memoization:
    # >>> memo = disable
    """
    def inner(*args, **kwargs):
        return func(*args, **kwargs)
    return inner


def decorator(decorator):
    """
    Decorate a decorator so that it inherits the docstrings
    and stuff from the function it's decorating.
    """

    def wrapped(f):
        return update_wrapper(decorator(f), f)
    return update_wrapper(wrapped, decorator)


@decorator
def countcalls(func):
    """Decorator that counts calls made to the function decorated."""

    def inner(*args):
        inner.calls += 1
        func.calls = inner.calls
        return func(*args)

    inner.calls = 0
    return inner


@decorator
def memo(func):
    """
    Memoize a function so that it caches all return values for
    faster future lookups.
    """
    memo_map = {}

    # @decorator(func)
    def wrapper(*args, **kwargs):
        key = (args, tuple(sorted(kwargs)))
        if key not in memo_map.keys():
            memo_map[key] = func(*args, **kwargs)
        return memo_map[key]

    return wrapper


@decorator
def n_ary(func):
    """
    Given binary function f(x, y), return an n_ary function such
    that f(x, y, z) = f(x, f(y,z)), etc. Also allow f(x) = x.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        if len(args) > 2:
            args = list(args)
            return func(args[-3], func(args[-2], args[-1]))
        else:
            return func(*args, **kwargs)

    return wrapper


@decorator
def trace(decarg):
    """Trace calls made to function decorated.
    @trace("____")
    def fib(n):
        ....

    # >>> fib(3)
     --> fib(3)
    ____ --> fib(2)
    ________ --> fib(1)
    ________ <-- fib(1) == 1
    ________ --> fib(0)
    ________ <-- fib(0) == 1
    ____ <-- fib(2) == 2
    ____ --> fib(1)
    ____ <-- fib(1) == 1
     <-- fib(3) == 3

    """

    def decorator_r(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            func.countii += 1
            print(decarg * func.countii + ' --> ' + func.__name__, '(', str(*args, **kwargs).strip(), ')')
            result = func(*args, **kwargs)
            print(decarg * func.countii + ' <-- ' + func.__name__, '(', str(*args, **kwargs).strip(),
                  ') == ' + str(result))
            func.countii -= 1
            return func(*args, **kwargs)

        func.countii = 0
        return wrapper

    return decorator_r


@memo
@countcalls
@n_ary
def foo(a, b):
    return a + b


@countcalls
@memo
@n_ary
def bar(a, b):
    return a * b


@countcalls
@trace("####")
@memo
def fib(n):
    """Some doc"""
    return 1 if n <= 1 else fib(n - 1) + fib(n - 2)


def main():
    print(foo(4, 3))
    print(foo(4, 3, 2))
    print(foo(4, 3))
    print("foo was called", foo.calls, "times")

    print(bar(4, 3))
    print(bar(4, 3, 2))
    print(bar(4, 3, 2, 1))
    print("bar was called", bar.calls, "times")

    print(fib.__doc__)
    fib(3)
    print("fib was called", fib.calls, "times")


if __name__ == '__main__':
    main()
